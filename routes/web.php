<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resources([
    'building' => 'BuildingController',
    'agreementbc' => 'AgreementBCController',
    'agreementbcdetail' => 'AgreementBCDetailController',
    'agreementa' => 'AgreementAController',
    'agreementadetail' => 'AgreementADetailController',
]);

Route::get('/agreementacek','AgreementAController@cek');
Route::get('/agreementbccek','AgreementBCController@cek');

Route::get('/agreementanotdone/{id}','AgreementAController@notdone');
Route::get('/agreementbcnotdone/{id}','AgreementBCController@notdone');
