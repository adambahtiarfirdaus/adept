<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementADetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_a_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_agreement_a')->unsigned();
            //A
            $table->string('A1')->nullable();
            $table->string('RA1')->nullable();
            $table->string('A2')->nullable();
            $table->string('RA2')->nullable();
            $table->string('A3')->nullable();
            $table->string('RA3')->nullable();
            $table->string('A4')->nullable();
            $table->string('RA4')->nullable();
            $table->string('A5')->nullable();
            $table->string('RA5')->nullable();
            $table->string('A6')->nullable();
            $table->string('RA6')->nullable();
            //B
            $table->string('B1')->nullable();
            $table->string('RB1')->nullable();
            $table->string('B2')->nullable();
            $table->string('RB2')->nullable();
            $table->string('B3')->nullable();
            $table->string('RB3')->nullable();
            $table->string('B4')->nullable();
            $table->string('RB4')->nullable();
            //C
            $table->string('C1')->nullable();
            $table->string('RC1')->nullable();
            $table->string('C2')->nullable();
            $table->string('RC2')->nullable();
            $table->string('C3')->nullable();
            $table->string('RC3')->nullable();
            $table->string('C4')->nullable();
            $table->string('RC4')->nullable();
            $table->string('C5')->nullable();
            $table->string('RC5')->nullable();
            //D
            $table->string('D1')->nullable();
            $table->string('RD1')->nullable();
            $table->string('D2')->nullable();
            $table->string('RD2')->nullable();
            $table->string('D3')->nullable();
            $table->string('RD3')->nullable();
            //E
            $table->string('E1')->nullable();
            $table->string('RE1')->nullable();
            $table->string('E2')->nullable();
            $table->string('RE2')->nullable();
            $table->string('E3')->nullable();
            $table->string('RE3')->nullable();
            //F
            $table->string('F1')->nullable();
            //G
            $table->string('G1')->nullable();
            //REMARKS
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_a_details');
    }
}
