<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Building;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //user
        User::create([
            'name' => 'admin',
            'email'=> 'admin@mail.com',
            'password' => Hash::make('admin123'),
            'role' => 'admin',
        ]);
        User::create([
            'name' => 'Divisi A',
            'email'=> 'divisiA@mail.com',
            'password' => Hash::make('divisiA123'),
            'role' => 'a',
        ]);
        User::create([
            'name' => 'Divisi B',
            'email'=> 'divisiB@mail.com',
            'password' => Hash::make('divisiB123'),
            'role' => 'b',
        ]);
        User::create([
            'name' => 'Divisi C',
            'email'=> 'divisiC@mail.com',
            'password' => Hash::make('divisiC123'),
            'role' => 'c',
        ]);

        //building
        Building::create([
            'name' => 'PT Sinar Jaya Building',
            'company'=> 'PT Sinar Jaya ',
            'pic' => 'Ujang Maman',
            'contact' => '0895617707462',
            'email' => 'sinarjaya@mail.com',
        ]);
    }
}
