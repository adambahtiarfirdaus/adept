@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Finding')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-search bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Finding')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">building</a> </li>
@endsection

{{-- content --}}
@section('content')
<div class="row">
    <div class="col-md-12">

        {{-- <style>
            a { text-decoration: none; } 
            a:hover { text-decoration: none; } 
        </style> --}}
        {{-- <div class="card wz">
            <div class="card-header">
                <h5>Verticle Wizard</h5>
                <span>Add class of <code>.form-control</code> with
                    <code>&lt;input&gt;</code> tag</span>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <div id="wizardb" class=''>
                            <section>
                                <form class="wizard-form run-form" id="verticle-wizard"
                                    action="{{route('agreementadetail.store')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id_agreement_a" value="{{$cek_id}}">
                                    <h3> Fire Alarm </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Fire Alarm</h5>
                                            <hr>
                                            
                                            <table class="table">
                                                <tr class="thead-dark">
                                                    <th >*Manual Cellpoint</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA1">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Smoke Detector</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A2">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA2">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Heat Detector</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A3">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA3">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Alarm Bells</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A4">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA4">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Main / Sub Alarm Panel</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A5">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA5">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Signal Notify By : </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="A6">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RA6">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3> Sprinkler System </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Sprinkler System</h5>
                                            <hr>
                                            
                                            <table class="table">
                                                <tr class="thead-dark">
                                                    <th >*Pump & Control </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="B1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RB1">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Control Valve</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="B2">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RB2">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Water Tank</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="B3">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RB3">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Sprinkler Breecing Inlet (if any)</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="B4">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RB4">
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3> Hosereel System </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Hosereel System</h5>
                                            <hr>
                                            
                                            <table class="table">
                                                <tr class="thead-dark">
                                                    <th >*Hosereel pressure test</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="C1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RC1">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr class="thead-dark">
                                                    <th >*Rubber hose</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="C2">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RC2">
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr class="thead-dark">
                                                    <th >*Stop valve</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="C3">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RC3">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr class="thead-dark">
                                                    <th >*Nozzle</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="C4">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RC4">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="thead-dark">
                                                    <th >*Pump & Control </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="C1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RC1">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3> Wet Riser System </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3"> Wet Riser System</h5>
                                            <hr>
                                            
                                            <table class="table">
                                                <tr class="thead-dark">
                                                    <th >*Pump & Control</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="D1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RD1">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr class="thead-dark">
                                                    <th >*Water Tank</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="D2">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RD2">
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr class="thead-dark">
                                                    <th >*Wet Riser Breeching Inlet (if any)</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="D3">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RD3">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3>Dry Riser System</h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Dry Riser System</h5>
                                            <hr>
                                            <table class="table">
                                                <tr class="thead-dark">
                                                    <th >*Dry Riser Pressure test</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="E1">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RE1">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr class="thead-dark">
                                                    <th >*Outlet Valve</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="E2">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RE2">
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr class="thead-dark">
                                                    <th >*Dry Riser Breeching Inlet (if any)</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select id="my-select" class="form-control" name="E3">
                                                                <option disabled selected>-----</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                                <option>N/A</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Remark</label>
                                                            <input id="my-input" class="form-control" type="text" name="RE3">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3>Fire Extingusiher</h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Fire Extingusiher</h5>
                                            <hr>
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Checked & Serviced On</label>
                                                            <input id="my-input" class="form-control" type="text" name="F1">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3> Other System </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Other System</h5>
                                            <hr>
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="">Checked & Serviced On</label>
                                                            <input id="my-input" class="form-control" type="text" name="G1">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <h3> Remarks </h3>
                                    <fieldset>
                                        <div class="">
                                            <h5 class="mt-3">Remarks</h5>
                                            <hr>
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <label for="my-textarea">Remarks</label>
                                                            <textarea id="my-textarea" class="form-control" name="remark" rows="3"></textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </form>
                            </section>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div> --}}



        <form action="{{route('agreementadetail.store')}}" method="POST">
        @csrf
        <input type="hidden" name="id_agreement_a" value="{{$cek_id}}">
        {{-- Fire Alarm --}}
        <div class="modal" id="firealarm">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
          
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">Fire Alarm</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
          
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <table class="table">
                            <tr class="thead-dark">
                                <th >*Manual Cellpoint</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A1">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA1">
                                    </div>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th >*Smoke Detector</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A2">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA2">
                                    </div>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th >*Heat Detector</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A3">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA3">
                                    </div>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th >*Alarm Bells</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A4">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA4">
                                    </div>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th >*Main / Sub Alarm Panel</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A5">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA5">
                                    </div>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th >*Signal Notify By : </th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select id="my-select" class="form-control" name="A6">
                                            <option disabled selected>-----</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                            <option>N/A</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="">Remark</label>
                                        <input id="my-input" class="form-control" type="text" name="RA6">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
          
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
          
              </div>
            </div>
          </div>
          {{-- Fire Alarm --}}
          {{-- Hosereel System --}}
          <div class="modal" id="hs">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
              
                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Hosereel System</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
              
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                                <table class="table">
                                    <tr class="thead-dark">
                                        <th >*Hosereel pressure test</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="C1">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RC1">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="thead-dark">
                                        <th >*Rubber hose</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="C2">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RC2">
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr class="thead-dark">
                                        <th >*Stop valve</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="C3">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RC3">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="thead-dark">
                                        <th >*Nozzle</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="C4">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RC4">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="thead-dark">
                                        <th >*Pump & Control </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="C1">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RC1">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                        </div>
                        
                    </div>
              
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
              
                  </div>
                </div>
              </div>
              


              <div class="modal" id="wr">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Wet Riser System </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <table class="table">
                                    <tr class="thead-dark">
                                        <th >*Pump & Control</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="D1">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RD1">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="thead-dark">
                                        <th >*Water Tank</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="D2">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RD2">
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr class="thead-dark">
                                        <th >*Wet Riser Breeching Inlet (if any)</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="D3">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RD3">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                        </div>
                        
                    </div>
                
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                
                    </div>
                </div>
                </div>

                <div class="modal" id="dr">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Dry Riser System </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                    
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">
                                <table class="table">
                                    <tr class="thead-dark">
                                        <th >*Dry Riser Pressure test</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="E1">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RE1">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="thead-dark">
                                        <th >*Outlet Valve</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="E2">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RE2">
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr class="thead-dark">
                                        <th >*Dry Riser Breeching Inlet (if any)</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select id="my-select" class="form-control" name="E3">
                                                    <option disabled selected>-----</option>
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    <option>N/A</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Remark</label>
                                                <input id="my-input" class="form-control" type="text" name="RE3">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            
                        </div>
                    
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    
                        </div>
                    </div>
                    </div>

                    <div class="modal" id="fe">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Fire Extingusiher</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                        
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="form-group">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="">Checked & Serviced On</label>
                                                    <input id="my-input" class="form-control" type="text" name="F1">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                        
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        
                            </div>
                        </div>
                    </div>

                    <div class="modal" id="os">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Other System</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                        
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="form-group">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="">Checked & Serviced On</label>
                                                    <input id="my-input" class="form-control" type="text" name="G1">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                        
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        
                            </div>
                        </div>
                    </div>

                    <div class="modal" id="r">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Remarks</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                        
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="form-group">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label for="my-textarea">Remarks</label>
                                                    <textarea id="my-textarea" class="form-control" name="remark" rows="3"></textarea>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                        
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        
                            </div>
                        </div>
                    </div>


              


        
        <a href="" data-toggle="modal" data-target="#firealarm">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Fire Alarm</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#hs" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Hosereel System</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#wr" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Water Riser System</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#dr" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Dry Riser System</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#fe" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Fire Extingusiher</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#os" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Other System</h3>
                </div>
            </div>
        </a>
        <a href="" class="mb-3" data-toggle="modal" data-target="#r" >
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title"><i class="feather icon-alert-circle"></i>&nbsp;&nbsp;Remarks</h3>
                </div>
            </div>
        </a>

        <input type="submit" value="Save" class="btn btn-lg btn-block btn-primary">
        
        </form>
        <div class="load">
            <center>
            <h1>Wait.......</h1>
            <img src="{{asset('image/load.gif')}}" alt="">
            </center>
        </div>
        @section('scriptpage')
            <script>
                $(document).ready(function(){
                    $('.load').hide();
                    $('a[href^="#finish"]').click(function(){
                        $('.run-form').submit();
                        $('.wz').hide();
                        $('.load').show();
                    });
                });
            </script>
        @endsection
    </div>
</div>
@endsection


