@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Agreement')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-file bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Agreement')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">Agreement</a> </li>
@endsection

{{-- content --}}
@section('content')
<div class="alert alert-lg alert-warning">
    <h1><i class="fa fa-exclamation-triangle "></i>You Already Have an agreement</h1>
    <span><a href="{{route('agreementadetail.edit',$data)}}">Click Here To Process</a></span>
</div>
@endsection

