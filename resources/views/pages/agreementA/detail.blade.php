<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
    
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
</head>
<body style="">
    <div class="row ">
        <div class="col-md-12">
            <br><br>
            <table class="table table-responsive" id="example">
                <thead>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    {{-- 1 --}}
                    <tr class="thead-dark">
                        <th>Fire Alarm</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>Manual Cellpoint</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Manual Cellpoint</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Alarm Bells</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Heat Detector</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Main / Sub Alarm Panel</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Signal Notify By</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>

                    {{-- 2 --}}
                    <tr class="thead-dark">
                        <th>Hosereel System</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>Hosereel pressure test</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Rubber Hose</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Stop Valve</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Nozzle</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Pump & Control</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>

                    {{-- 3 --}}
                    <tr class="thead-dark">
                        <th>Wet Riser System</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>Pump & Control</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                            
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Water Tank</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Wet Riser Breeching Inlet (if Any)</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    {{-- 4 --}}
                    <tr class="thead-dark">
                        <th>Dry Riser System</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>Dry Riser pressure test</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Outlet Valve</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    <tr>
                        <td>Dry Riser Breeching Inlet (if Any)</td>
                        <td>
                            @if ($data->A1 == 'Yes')
                            <span class="badge badge-success">Yes</span>
                            @else
                            <span class="badge badge-danger">No</span>
                            @endif
                           
                        </td>
                        <td>{{$data->RA1}}</td>
                    </tr>
                    {{-- 5 --}}
                    <tr class="thead-dark">
                        <th>Dry Riser System</th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf'
            ],
            "searching": false,
            "paging": false,
            "info": false,
            "bSort" : false
        } );
    } );
</script>