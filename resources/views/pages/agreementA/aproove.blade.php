@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Agreement')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-file bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Agreement')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">Agreement</a> </li>
@endsection

{{-- content --}}
@section('content')
<form method="post" action="{{route('agreementa.update',$data->id)}}">
    @csrf  @method('patch')
<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <h5>Form Agreement</h5>
                <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
            </div>
            <div class="card-block">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Building</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly value="{{$data->building->name}}"  >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Location</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly value="{{$data->location}}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Report</label>
                        <div class="col-sm-10">
                            <a href="{{route('agreementadetail.show',$data)}}" class="btn btn-success  "><i class=" feather icon-file-text"></i>Report</a>
                        </div>
                    </div>
            </div>
        </div>
        <style>
        .sign{
         background-color: white;
        }
        </style>
        <div class="row  justify-content-center align-items-center" >
            <div class="col-md-6">
                <center>
                <p>Client</p>
                <canvas id="signature-pad1" class="sign card signature-pad" width="200%"  required name="signature"></canvas>
                </center>
                
            </div>
            <div class="col-md-6">
                <center>
                <p>Technican</p>
                <canvas id="signature-pad2" class="sign card signature-pad" width="200%"  required name="signature"></canvas>
                </center>
                
            </div>
          </div>
          
          <input type="submit" value="Save" id="save" class="btn btn-block btn-primary">
    </div>
</div>
</form>
@endsection

@section('scriptpage')
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
  var canvas1 = document.querySelector("#signature-pad1");
  var signaturePad1 = new SignaturePad(canvas1);
  var canvas2 = document.querySelector("#signature-pad2");
  var signaturePad2 = new SignaturePad(canvas2);
</script>
<script>
    $(document).ready(function() {
        $('#building').change(function(){
        });
        $('#save').click(function(){
            $.ajax({
                url: "/saveagreementBC",
                type: "post",
                data: {'building':$('input[name=building]').val(), 'location': $('input[name=lokation]').val()},
                dataType: 'JSON',
                success: function (data) {
                console.log(data);

                });
            });
        });
    });
</script>
@endsection
