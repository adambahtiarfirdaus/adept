@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Edit Building')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-edit bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Edit Building')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">Edit building</a> </li>
@endsection

{{-- content --}}
@section('content')
<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <h5>Edit Building</h5>
                <a href="" class="btn btn-secondary btn-sm btn-round float-right "><i class="feather icon-arrow-left"></i>  back</a>
            </div>
            <div class="card-block">
                <form method="post" action="{{route('building.update',$data)}}">
                    @csrf @method('patch')
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Building</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Company</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="company" value="{{$data->company}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">PIC</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="pic" value="{{$data->pic}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contact</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="contact" value="{{$data->contact}}">
                        </div>
                    </div>
                    <input type="submit" value="Update" class="btn btn-block btn-primary">
                </form>
                
            </div>
        </div>
    </div>
</div>
@endsection

