@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Building')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-grid bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Building')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">building</a> </li>
@endsection

{{-- content --}}
@section('content')
<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <h5>Building</h5>
                <a href="{{route('building.create')}}" class="btn btn-success btn-sm btn-round float-right "><i class="feather icon-plus"></i> Add</a>
            </div>
            <div class="card-block">
                <table id="alt-pg-dt" class="table table-striped table-responsive table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Building</th>
                            <th>Company</th>
                            <th>PIC</th>
                            <th>Contact</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach ($data as $item)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->company }}</td>
                            <td>{{ $item->pic }}</td>
                            <td>{{ $item->contact }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-6">
                                        <a href class="btn btn-sm btn-info feather icon-edit"><i class=""></i></a>
                                    </div>
                                    <div class="col-6">
                                        <form action="{{route('building.destroy',$item)}}" method="POST">
                                        @csrf @method('delete')
                                            <button href="" class="btn btn-sm btn-danger feather icon-trash"><i class=""></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


