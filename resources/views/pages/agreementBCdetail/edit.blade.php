@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Finding')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-search bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Finding')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">building</a> </li>
@endsection

{{-- content --}}
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="form">
            <div class="card">
                <div class="card-header">
                    <h5>Finding?</h5>
                    <button type="button" class="btn btn-success btn-sm btn-round float-right" data-toggle="modal" data-target="#exampleModalCenter">
                        <i class="feather icon-plus"></i> Add
                    </button>
                    <form action="{{route('agreementbcdetail.store')}}" method="post"   enctype="multipart/form-data" >
                    @csrf
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Finding?</h5>
                                        <button type="button" class="close form-modal" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-modal">
                                            <input type="hidden" name="id_agreement_bc" value="{{$cek_id}}">
                                            <label for="">Picture </label>
                                            <input required type="file" capture="camera" class="dropify" data-height="300" name="report" data-max-file-size="10M"   />
                                            <div class="form-group">
                                                <label for="my-input">Location</label>
                                                <input id="my-input" class="form-control" type="text" name="location">
                                            </div>
                                            <div class="form-group">
                                                <label for="my-textarea">Description</label>
                                                <textarea id="my-textarea" class="form-control" name="description" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="load-modal">
                                                <center>
                                                    <h1>Wait...</h1>
                                                    <img src="{{asset('image/load.gif')}}" alt="" >
                                                </center>
                                           </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="form-modal">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="save-modal">Save </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
            @foreach ($data as $item)
                <div class="card">
                    <img class="card-img" src="{{asset('photobc/'.$item->path)}}" alt="">
                    <div class="card-img-overlay">
                        <form action="{{route('agreementbcdetail.destroy',$item)}}" method="post">
                        @csrf @method('delete')
                        <button type="submit" class="float-right btn btn-round btn-danger feather icon-x-circle"></button>
                        </form>
                        <h5 class="card-title text-white">{{ $item->location }}</h5>
                        <p class="card-text text-white">{{ $item->description }}</p>
                    </div>
                </div>
               
            @endforeach
            {{ $data->links() }}
    
            <?php
                if(count($data) > 0){
                
            ?>
            <form action="{{route('agreementbc.update',$cek_id)}}" method="post">
                @csrf @method('patch')
                <input type="submit" value="Done" class="btn btn-primary btn-block btn-lg" id="save">
            </form>
            <?php }?>
        </div>
       <div class="load">
            <center>
                <h1>Wait.......</h1>
                <img src="{{asset('image/load.gif')}}" alt="" width="300">
            </center>
       </div>
    </div>
</div>
@endsection

@section('scriptpage')
    <script>
        $(document).ready(function(){
            $('.load').hide();
            $('.load-modal').hide();
            $('#save-modal').click(function(){
                $('.form-modal').hide();
                $('.load-modal').show();
            });
            $('#save').click(function(){
                $('.form').hide();
                $('.load').show();
            });
        });
    </script>
@endsection


