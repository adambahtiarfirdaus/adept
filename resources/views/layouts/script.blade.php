
<script type="text/javascript" src="{{asset('')}}"></script>
<script type="text/javascript" src="{{asset('')}}files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{asset('')}}files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="{{asset('')}}files/bower_components/bootstrap/js/bootstrap.min.js"></script>

<script src="{{asset('')}}files/assets/pages/waves/js/waves.min.js"></script>

<script type="text/javascript" src="{{asset('')}}files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="{{asset('')}}files/assets/js/pcoded.min.js"></script>
<script src="{{asset('')}}files/assets/js/vertical/vertical-layout.min.js"></script>

    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('fil es/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('files/bower_components/dropify/js/dropify.min.js')}}"></script>


<script src="{{asset('files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{asset('files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{asset('files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('files/assets/pages/form-validation/validate.js')}}"></script>
<script src="{{asset('files/assets/pages/forms-wizard-validation/form-wizard.js')}}"></script>

<script type="text/javascript" src="{{asset('files/assets/js/bootstrap-growl.min.js')}}"></script>
    {{-- <script type="text/javascript" src="{{asset('files/assets/pages/notification/notification.js')}}"></script> --}}

<script type="text/javascript" src="{{asset('files/assets/js/script.min.js')}}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
    </script>
@if(session('message'))
<input type="hidden" name="" id="alertnya" value="{{session('message')}}">
<script>
        'use strict';
        var alertnya = document.getElementById('alertnya').value;
        $(document).ready(function() {
        //Welcome Message (not for login page)
        function notify(message, type){
            $.growl({
                message: message
            },{
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse bg-success',
                placement: {
                    from: 'top',
                    align: 'center'
                },
                delay: 2500,
                animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        };
    
       
         notify(alertnya, 'inverse');
       
    });
</script>
@endif
    @yield('scriptpage')
    