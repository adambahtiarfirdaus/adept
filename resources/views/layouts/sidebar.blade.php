<div class="pcoded-navigation-label">Main Menu</div>
<ul class="pcoded-item pcoded-left-item">
    @if (Auth::user()->role == 'admin') 
    <li class=" ">
        <a href="{{url('/home')}}" class="waves-effect waves-dark">
            <span class="pcoded-micon">
                <i class="feather icon-home"></i>
            </span>
            <span class="pcoded-mtext">Dashboard</span>
        </a>
    </li>
    <li class=" ">
        <a href="{{route('building.index')}}" class="waves-effect waves-dark">
            <span class="pcoded-micon">
                <i class="feather icon-grid"></i>
            </span>
            <span class="pcoded-mtext">Building</span>
        </a>
    </li>
    @endif
    @if (Auth::user()->role == 'a') 
        <li class=" ">
            <a href="{{route('agreementa.index')}}" class="waves-effect waves-dark">
                <span class="pcoded-micon">
                    <i class="feather icon-file"></i>
                </span>
                <span class="pcoded-mtext">Agreement </span>
            </a>
        </li>    
    @endif
    
    @if (Auth::user()->role == 'b' || Auth::user()->role == 'c') 
    <li class=" ">
            <a href="{{route('agreementbc.index')}}" class="waves-effect waves-dark">
                <span class="pcoded-micon">
                    <i class="feather icon-file"></i>
                </span>
                <span class="pcoded-mtext">Agreement</span>
            </a>
        </li>    
    @endif
    

</ul>