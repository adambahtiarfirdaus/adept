
@extends('layouts.app')

{{-- title-dashboard --}}
@section('title','Dashboard')

{{-- icon-page --}}
@section('iconpage')
<i class="feather icon-home bg-c-blue"></i>
@endsection

{{-- title-dashboard --}}
@section('titlepage','Dashboard')

{{-- title-dashboard --}}
@section('descpage','Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem necessitatibus dolorem beatae')

{{-- breadcrumb --}}
@section('breadcrumb')
 <li class="breadcrumb-item">
    <a href="index-2.html"><i class="feather icon-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#!">Dashboard</a> </li> 
@endsection

{{-- content --}}
@section('content')
<div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card prod-p-card card-red">
            <div class="card-body">
                <div class="row align-items-center m-b-30">
                    <div class="col">
                        <h6 class="m-b-5 text-white">Total Profit</h6>
                        <h3 class="m-b-0 f-w-700 text-white">$1,783</h3>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-money-bill-alt text-c-red f-18"></i>
                    </div>
                </div>
                <p class="m-b-0 text-white"><span
                        class="label label-danger m-r-10">+11%</span>From
                    Previous Month</p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card prod-p-card card-blue">
            <div class="card-body">
                <div class="row align-items-center m-b-30">
                    <div class="col">
                        <h6 class="m-b-5 text-white">Total Orders</h6>
                        <h3 class="m-b-0 f-w-700 text-white">15,830</h3>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-database text-c-blue f-18"></i>
                    </div>
                </div>
                <p class="m-b-0 text-white"><span
                        class="label label-primary m-r-10">+12%</span>From
                    Previous Month</p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card prod-p-card card-green">
            <div class="card-body">
                <div class="row align-items-center m-b-30">
                    <div class="col">
                        <h6 class="m-b-5 text-white">Average Price</h6>
                        <h3 class="m-b-0 f-w-700 text-white">$6,780</h3>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-dollar-sign text-c-green f-18"></i>
                    </div>
                </div>
                <p class="m-b-0 text-white"><span
                        class="label label-success m-r-10">+52%</span>From
                    Previous Month</p>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card prod-p-card card-yellow">
            <div class="card-body">
                <div class="row align-items-center m-b-30">
                    <div class="col">
                        <h6 class="m-b-5 text-white">Product Sold</h6>
                        <h3 class="m-b-0 f-w-700 text-white">6,784</h3>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-tags text-c-yellow f-18"></i>
                    </div>
                </div>
                <p class="m-b-0 text-white"><span
                        class="label label-warning m-r-10">+52%</span>From
                    Previous Month</p>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-md-6">
        <div class="card o-hidden">
            <div class="card-header">
                <h5>Total Leads</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-4">
                        <p class="text-muted m-b-5">Overall</p>
                        <h6>68.52%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Monthly</p>
                        <h6>28.90%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Day</p>
                        <h6>13.50%</h6>
                    </div>
                </div>
            </div>
            <div id="sal-income" style="height:100px"></div>
        </div>
    </div>
    <div class="col-xl-4 col-md-6">
        <div class="card o-hidden">
            <div class="card-header">
                <h5>Total Vendors</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-4">
                        <p class="text-muted m-b-5">Overall</p>
                        <h6>68.52%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Monthly</p>
                        <h6>28.90%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Day</p>
                        <h6>13.50%</h6>
                    </div>
                </div>
            </div>
            <div id="rent-income" style="height:100px"></div>
        </div>
    </div>
    <div class="col-xl-4 col-md-12">
        <div class="card o-hidden">
            <div class="card-header">
                <h5>Invoice Generate</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-4">
                        <p class="text-muted m-b-5">Overall</p>
                        <h6>68.52%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Monthly</p>
                        <h6>28.90%</h6>
                    </div>
                    <div class="col-4">
                        <p class="text-muted m-b-5">Day</p>
                        <h6>13.50%</h6>
                    </div>
                </div>
            </div>
            <div id="income-analysis" style="height:100px"></div>
        </div>
    </div>
</div>
@endsection
