<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementA extends Model
{
    protected $guarded = [];

    public function building()
    {
        return $this->hasOne('App\Building','id','id_building');
    }
}
