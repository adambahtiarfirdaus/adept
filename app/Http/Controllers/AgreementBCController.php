<?php

namespace App\Http\Controllers;

use App\AgreementBC;
use App\Building;
use Auth;
use Illuminate\Http\Request;

class AgreementBCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cek = AgreementBC::where(['id_user' => Auth::user()->id,'status' =>'waiting' ])->get();
        if (count($cek)  > 0) {
            return redirect('/agreementbccek');
        }else{
            return view('pages.agreementBC.index',['data'=>AgreementBC::where('id_user',Auth::user()->id),'building'=>Building::all()]);
        }
    }
    public function cek()
    {
        $cek = AgreementBC::where('id_user',Auth::user()->id)->first();
        return view('pages.agreementBC.cek',['data' => $cek]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = AgreementBC::create($request->all());
        return redirect()->route('agreementbcdetail.edit',$data)->with('message','Input Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgreementBC  $agreementBC
     * @return \Illuminate\Http\Response
     */
    public function show(AgreementBC $agreementBC)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgreementBC  $agreementBC
     * @return \Illuminate\Http\Response
     */
    public function edit(AgreementBC $agreementBC)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgreementBC  $agreementBC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        AgreementBC::where('id',$id)->update([
            'status'=>'done'
        ]);
        return redirect()->route('agreementbc.index')->with('message','Success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgreementBC  $agreementBC
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgreementBC $agreementBC)
    {
        
    }
}
