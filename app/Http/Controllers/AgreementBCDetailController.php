<?php

namespace App\Http\Controllers;

use App\AgreementBCDetail;
use Illuminate\Http\Request;

class AgreementBCDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('report')){

            
            $raw = $request->file('report');
            $file = $raw->getClientOriginalName();

            $request->file('report')->move('photobc',$file);
            AgreementBCDetail::create([
                'location' =>$request->location,
                'id_agreement_bc' =>$request->id_agreement_bc,
                'description' =>$request->description,
                'path' =>$file,
            ]);
            
            return redirect()->route('agreementbcdetail.edit',$request->id_agreement_bc)->with('message','Input Success');
        }else{
            return back()->with('message','Picture is required');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgreementBCDetail  $agreementBCDetail
     * @return \Illuminate\Http\Response
     */
    public function show(AgreementBCDetail $agreementBCDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgreementBCDetail  $agreementBCDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.agreementBCdetail.edit',['data'=>AgreementBCDetail::where('id_agreement_bc',$id)->paginate('3'),'cek_id'=>$id ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgreementBCDetail  $agreementBCDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgreementBCDetail $agreementBCDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgreementBCDetail  $agreementBCDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AgreementBCDetail::find($id)->delete();
        return back()->with('message','Deleted');
    }
}
