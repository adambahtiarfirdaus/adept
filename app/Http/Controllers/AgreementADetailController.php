<?php

namespace App\Http\Controllers;

use App\AgreementADetail;
use Illuminate\Http\Request;
use App\AgreementA;
class AgreementADetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = AgreementADetail::create($request->all());
        AgreementA::where('id',$data->id_agreement_a)->update(['status'=> 'notdone']);
        return redirect()->route('agreementa.index')->with('message','Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgreementADetail  $agreementADetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = AgreementADetail::find($id);
        return view('pages.agreementa.detail',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgreementADetail  $agreementADetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.agreementAdetail.edit',['cek_id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgreementADetail  $agreementADetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgreementADetail $agreementADetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgreementADetail  $agreementADetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgreementADetail $agreementADetail)
    {
        //
    }
}
