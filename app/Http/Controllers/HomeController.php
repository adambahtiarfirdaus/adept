<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (Auth::user()->role == 'admin') {
            return view('home');
        }elseif (Auth::user()->role == 'a') {
            return redirect()->route('agreementa.index');
        }elseif (Auth::user()->role == 'b' || Auth::user()->role == 'c') {
            return redirect()->route('agreementbc.index');
        }
    }
}
