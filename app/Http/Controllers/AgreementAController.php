<?php

namespace App\Http\Controllers;

use App\AgreementA;
use App\Building;
use Auth;
use Illuminate\Http\Request;

class AgreementAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cek = AgreementA::where(['id_user' => Auth::user()->id,'status' =>'waiting'])->get();

        $cek1 = AgreementA::where(['id_user' => Auth::user()->id,'status' =>'notdone'])->first();
        if (count($cek)  > 0) {
            return redirect('/agreementacek');
        }
        else{
            if ($cek1) {
                return redirect('/agreementanotdone/'.$cek1->id);
            }else{
                return view('pages.agreementA.index',['data'=>AgreementA::where('id_user',Auth::user()->id),'building'=>Building::all()]);
            }
            
        }
        
        
    }
    public function cek()
    {
        $cek = AgreementA::where('id_user',Auth::user()->id)->first();
        return view('pages.agreementA.cek',['data' => $cek]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = AgreementA::create($request->all());
        return redirect()->route('agreementadetail.edit',$data)->with('message','Input Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgreementA  $agreementA
     * @return \Illuminate\Http\Response
     */
    public function show(AgreementA $agreementA)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgreementA  $agreementA
     * @return \Illuminate\Http\Response
     */
    public function edit(AgreementA $agreementA)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgreementA  $agreementA
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        AgreementA::where('id',$id)->update(['status'=> 'done']);

        return redirect()->route('agreementa.index')->with('message','Done');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgreementA  $agreementA
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgreementA $agreementA)
    {
        //
    }
    public function notdone($id)
    {
        $data = AgreementA::where('id',$id)->first();
        return view('pages.agreementa.aproove',['data'=>$data]);
    }
}
