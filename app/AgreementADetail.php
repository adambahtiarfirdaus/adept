<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementADetail extends Model
{
    protected $guarded = [];


    public function agreement()
    {
        return $this->hasOne('App\AgreementA','id','id_agreement');
    }
}
